<?php

namespace App;

class ProductDto
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $price;

    /**
     * @var string
     */
    private $vendorName;

    /**
     * @var string
     */
    private $vendorEmail;

    public function __construct(string $name, ?string $description, int $price, string $vendorName, string $vendorEmail)
    {
        // TODO $price should be decimal, in lack of such type and because of ASAP case I used it as integer
        $this->name        = $name;
        $this->description = $description;
        $this->price       = $price;
        $this->vendorName  = $vendorName;
        $this->vendorEmail = $vendorEmail;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getVendorName(): string
    {
        return $this->vendorName;
    }

    /**
     * @return string
     */
    public function getVendorEmail(): string
    {
        return $this->vendorEmail;
    }
}
