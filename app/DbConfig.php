<?php

namespace App;

// TODO extract configuration to env file
class DbConfig
{
    private const HOST = 'db';
    private const PORT = 3306;
    private const USERNAME = 'root';
    private const PASSWORD = 'ZF90VKCBsxCNy752'; // ohh, so bad to keep passwords like this, i know, but you gave just only 4 hours :(
    private const DATABASE = 'marketplace_backend_t8ke3y';
    private $pdo;

    public function __construct()
    {
        $this->pdo = new \PDO('mysql:host=' . self::HOST .
            ';port=' . self::PORT .
            ';dbname=' . self::DATABASE, self::USERNAME, self::PASSWORD);
    }

    public function getPDO(): \PDO
    {
        return $this->pdo;
    }
}
