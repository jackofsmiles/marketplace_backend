<?php

namespace App\Controllers;

use App\ProductDto;

class ProductValidator
{
    private $require = [
        'name', 'price', 'vendor_name', 'vendor_email',
    ];

    private $invalidParam;

    /**
     * ProductValidator constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        foreach ($this->require as $requiredParam) {
            if (!in_array($requiredParam, array_keys($this->params))) {
                $this->invalidParam = $requiredParam;

                return false;
//                throw new InvalidProductRequest("Key ${requiredParam} is required");
            }
        }

        return true;
    }

    public function getDto(): ProductDto
    {
        return new ProductDto($this->params['name'], $this->params['description'], $this->params['price'], $this->params['vendor_name'], $this->params['vendor_email']);
//        return new ProductDto(...array_values($this->params));
    }

    /**
     * @return mixed
     */
    public function getInvalidParam()
    {
        return $this->invalidParam;
    }
}
