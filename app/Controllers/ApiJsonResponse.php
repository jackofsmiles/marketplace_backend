<?php

namespace App\Controllers;

class ApiJsonResponse
{
    private $response = [
        'data' => [],
        'message' => '',
    ];

    /**
     * @var array
     */
    private $data;

    /**
     * @var null
     */
    private $message;

    /**
     * JsonResponse constructor.
     * @param array $data
     * @param null $message
     */
    public function __construct($data = [], $message = null)
    {
        $this->response['data']    = $data;
        $this->response['message'] = $message;
    }

    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->response['data'] = $data;
    }

    /**
     * @param null $message
     */
    public function setMessage($message): void
    {
        $this->response['message'] = $message;
    }
}
