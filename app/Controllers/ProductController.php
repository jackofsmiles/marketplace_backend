<?php

namespace App\Controllers;

use App\Services\ProductService;
use League\Route\Http\Exception\BadRequestException;
use Psr\Http\Message\ServerRequestInterface;

class ProductController
{
    public function addProduct(ServerRequestInterface $request)
    {
        // TODO check if there more convenient way to get body as json in phpleague pckg
        $params = json_decode($request->getBody()->getContents(), true);

        if (is_null($params)) {
            throw new BadRequestException('Error! Empty or invalid request');
        }

        $response = new ApiJsonResponse();

        // TODO use IOC container to DI objects
        $productService = new ProductService();

        $validator = new ProductValidator($params);

        if ($validator->validate()) {
            $productDto = $validator->getDto();
            $product    = $productService->addProduct($productDto);
            $a          = 1;

//            try {
//            } catch (InvalidProductRequest $exception) {
//                $response->setMessage('Error! Check request for invalid params', $validator->getInvalidParam());
//            } catch (\Exception $exception) {
//                $response->setMessage('Error! Check request params');
//            }
        } else {
            throw new BadRequestException('Error! Check request for invalid param: ' . $validator->getInvalidParam());
        }


        return $response->getResponse();
    }

    public function getProduct(ServerRequestInterface $request, array $args)
    {
        return [
            'mockup' => true
        ];
    }

    public function searchProducts(ServerRequestInterface $request)
    {
        return [
            'mockup' => true
        ];
    }

    // TODO sanitize input/output from/to db response & queries
}
