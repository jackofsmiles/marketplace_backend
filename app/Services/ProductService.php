<?php

namespace App\Services;

use App\DbConfig;
use App\ProductDto;

// TODO split on small services if will grow according to SRP ;)
class ProductService
{
    /**
     * @param ProductDto $productDto
     * @return bool
     */
    public function addProduct(ProductDto $productDto)
    {
        // TODO extract database layer to separate class or use orm
        $pdo = (new DbConfig)->getPDO();
        $sql = $pdo->prepare('INSERT INTO products (name, description, price, vendor_name, vendor_email) VALUES (?, ?, ?, ?, ?)');
        $sql->bindParam(1, $productDto->getName());
        $sql->bindParam(2, $productDto->getDescription());
        $sql->bindParam(3, $productDto->getPrice());
        $sql->bindParam(4, $productDto->getVendorName());
        $sql->bindParam(5, $productDto->getVendorEmail());

        return $sql->execute();
    }
}
