<?php declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use App\Controllers\ProductController;

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);
$router  = new League\Route\Router;

$responseFactory = new \Zend\Diactoros\ResponseFactory();

$strategy = new League\Route\Strategy\JsonStrategy($responseFactory);
$router   = (new League\Route\Router)->setStrategy($strategy);

// map a route
$router->post('/products/new', ProductController::class . '::addProduct');
$router->get('/products/search', ProductController::class . '::searchProducts');
$router->get('/products/{id}', ProductController::class . '::getProduct');

$response = $router->dispatch($request);

// send the response to the browser
(new Zend\HttpHandlerRunner\Emitter\SapiEmitter)->emit($response);
